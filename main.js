var schema = {
    "type": "object",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "kai-schema-test",
    "properties": {
        "fname":{"type":"string"},
        "lname": {"type":"string"},
        "age":{"type":"number"},
        "birtdate": {"type":"string", "format":"date"},
        "address":{"type":"array", "items":{"type":"string", }}
        }
    }

    var invaliddata = {
        "fname": "Kai",
        "lname": "Cooper",
        "age": 25,
        "birtdate": "2018-11-13",
        "address": ['1234', 'asdsda']
    }


function validateResponse(){
    const ajv = new Ajv();
    var result = ajv.validate(schema, invaliddata);
    if(ajv.errors){
        var msg = ajv.errors.map(function(ele){
            var result = ele['message'];
            return result;
        }) 
        emptyTarget('error')
        document.getElementById('error').append(JSON.stringify(msg))
    }else{
        emptyTarget('data')
        document.getElementById('data').append(JSON.stringify(invaliddata))
    }
}

function emptyTarget(obj){
    document.getElementById(obj).innerHTML = '';
}
